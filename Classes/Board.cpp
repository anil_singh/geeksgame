/*
 * Board.cpp
 *
 *  Created on: Oct 17, 2015
 *      Author: SINGHAK
 */

#include "Board.h"
#include "GamePlay.h"

Board::Board()
{
	// TODO Auto-generated constructor stub
	stepX = stepY = 10;
	size = Director::getInstance()->getWinSize();
	move_count = 0;
	loc_node = nullptr;
	//	turn = Turn::Player;
	//	this->setTouchEnabled(true);
}

Board::~Board() {
	// TODO Auto-generated destructor stub
}

void Board::setno_Grid(int no_grid)
{
	this->no_grid = no_grid;
	stepX = 150;
	stepY = 130;
}

int Board::getno_Grid()
{
	return no_grid;
}

void Board::createGrid()
{
	float y_change = 0;
	float x_change = 0;
	for (int i = 0; i < no_grid; ++i)
	{
		y_change = (size.height * .10) + (i * stepY);

		for (int j = 0; j < no_grid; ++j)
		{
			x_change = (size.width *.10) + (j * stepX);
			LocationNode* tem_loc = LocationNode::create();
			tem_loc->setnod_Cordinate(Vec2(i,j));
			tem_loc->setOcupied(false);
			tem_loc->setno_Character(0);
			tem_loc->set_Type(Charater_Type::None);
			tem_loc->setTag((no_grid*i)+j);
			log("In the Board create %d", tem_loc->getTag());
			tem_loc->setPosition(x_change , y_change);

			this->addChild(tem_loc);
		}
	}
}

void Board::setNodeChild(LocationNode* loc_node)
{
	//	if(turn == Turn::Player)
	move_count++;
	locNodeList.pushBack(loc_node);
	if(move_count == 2)
	{
		log("In the BoarChild %d", move_count);
		((GamePlay*)(this->getParent()))->onUserMove(locNodeList.front(), locNodeList.back());
	}
}

/*LocationNode* Board::getNodeChild()
{
	return loc_node;
}*/

Vec2 Board::setLocNodePos(int x, int y, int y_chang, LocationNode* loc_1)
{
	Vec2 pos;
	pos.x = loc_1->getPositionX() + loc_1->getBoundingBox().size.width + (x * stepX);
	pos.y = y_chang ;//+ (loc_1->getBoundingBox().size.width );
	return pos;
}
