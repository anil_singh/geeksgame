/*
 * GamePlay.h
 *
 *  Created on: Oct 17, 2015
 *      Author: SINGHAK
 */

#ifndef GAMEPLAY_H_
#define GAMEPLAY_H_
#include "cocos2d.h"
#include "Board.h"

USING_NS_CC;
using namespace std;


enum Turn {Computer, Player, Nobody};
class GamePlay : public Layer
{
private:
	int grid_size;
	int no_goat;
	int no_tiger;
	vector<Vec2> pos_lion, possible_moves;
	Size size;
	Board *board;
	Turn turn;
	int user_move_count;
	bool isGameOver;
	Texture2D *loin_tex, *goat_tex, *none_tex;
	Sprite *lion_sp, *goat_sp, *none_sp;
	LabelTTF* label;
	void initalPos();
	void menuCloseCallback(Ref* pSender);
	bool update_Tigermove(Vec2 prev, Vec2 cur);
	int intialiseChtr(Charater_Type type, int no_animal, int count=0);
public:
	GamePlay();
	virtual ~GamePlay();
	static Scene* createScene();
	virtual bool init();
	bool validate_general_move(Vec2 pos_1, Vec2 pos_2);
	bool validate_special_move(Vec2 pos_1, Vec2 pos_2);
	bool tiger_Move();
	void onUserMove(LocationNode* first, LocationNode * second);

	CREATE_FUNC(GamePlay);
};

#endif /* GAMEPLAY_H_ */
