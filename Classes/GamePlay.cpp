/*
 * GamePlay.cpp
 *
 *  Created on: Oct 17, 2015
 *      Author: SINGHAK
 */

#include "GamePlay.h"


GamePlay::GamePlay()
{
	// TODO Auto-generated constructor stub
	user_move_count = 0;
	turn = Turn::Player;
	isGameOver = true;
	board = nullptr;
}

GamePlay::~GamePlay()
{
	// TODO Auto-generated destructor stub
}

Scene* GamePlay::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = GamePlay::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

bool GamePlay::init()
{
	grid_size = 5;
	no_goat = 20;
	no_tiger = 1;
	turn = Turn::Player;

	size = Director::getInstance()->getVisibleSize();
	auto sprite = Sprite::create("bg.jpg");
	sprite->setPosition(Vec2(size.width/2 , size.height/2));
    this->addChild(sprite, 0);

	possible_moves.push_back(Vec2(2,2));
	possible_moves.push_back(Vec2(-2,2));
	possible_moves.push_back(Vec2(-2,-2));
	possible_moves.push_back(Vec2(2,-2));
	possible_moves.push_back(Vec2(0,2));
	possible_moves.push_back(Vec2(0,-2));
	possible_moves.push_back(Vec2(-2,0));
	possible_moves.push_back(Vec2(2,0));


	label = LabelTTF::create("No of Goat: "+Value(no_goat).asString(),"fonts\arial.ttf", 40);
	label->setPosition(size.width*.85, size.height * .90);
	this->addChild(label);

	board = Board::create();
	board->setno_Grid(grid_size);
	board->createGrid();
	board->setPosition(Vec2(size.width *.05, size.height * .05));
	board->setTag(1);
	this->addChild(board);
	initalPos();



	auto closeItem = MenuItemImage::create(
			"CloseNormal.png",
			"CloseSelected.png",
			CC_CALLBACK_1(GamePlay::menuCloseCallback, this));

	closeItem->setPosition(Vec2(size.width - closeItem->getContentSize().width/2 ,
			closeItem->getContentSize().height/2));

	// create menu, it's an autorelease object
	auto menu = Menu::create(closeItem, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu, 1);

	return true;
}

void GamePlay::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}



bool GamePlay::validate_general_move(Vec2 pos_1, Vec2 pos_2)
{
	bool isValid = true;
	int size = grid_size - 1;

	if(pos_1.x < 0 || pos_1.y < 0 || pos_2.x < 0 || pos_2.y < 0 )
		isValid = false;

	else if(pos_1.x > size || pos_1.y > size || pos_2.x > size || pos_2.y > size )
		isValid = false;

	else if(abs(pos_1.x - pos_2.x) > 1 || abs(pos_1.y - pos_2.y) > 1 )
		isValid = false;

	else if(pos_1.x == pos_2.x && pos_1.y == pos_2.y )
		isValid = false;

	log("In validate general %d", isValid);
	log("In validate general %f  %f", pos_1.x, pos_1.y);
	log("In validate general %f  #f", pos_2.x, pos_2.y);

	return isValid;
}

bool GamePlay::validate_special_move(Vec2 pos_1, Vec2 pos_2)
{
	log("In the GamePlay special_move_1");
	bool isValid = false;
	int size = grid_size - 1;
	int x, y, tag;
	LocationNode* temp;

	if(pos_1.x < 0 || pos_1.y < 0 || pos_2.x < 0 || pos_2.y < 0 )
		isValid = false;
	else if(pos_1.x > size || pos_1.y > size || pos_2.x > size || pos_2.y > size )
		isValid = false;
	else if(abs(pos_1.x - pos_2.x) == 2 && abs(pos_1.y - pos_2.y) == 2 )
		isValid = true;
	else if(abs(pos_1.x - pos_2.x) == 2 || abs(pos_1.y - pos_2.y) == 2)
	{
		if(abs(pos_1.x - pos_2.x) == 2 && abs(pos_1.y - pos_2.y) == 0)
			isValid = true;
		if(abs(pos_1.x - pos_2.x) == 0 && abs(pos_1.y - pos_2.y) == 2)
			isValid = true;
	}

	log("In the GamePlay special_move_2");
	if(isValid)
	{
		log("In the GamePlay special_move_3");
		x = (pos_1.x + pos_2.x)/2;
		y = (pos_1.y + pos_2.y)/2;
		tag = x*grid_size + y;
		log("In the GamePlay special_move_4 %d  %d",x,y);
		temp = (LocationNode*)(this->getChildByTag(1))->getChildByTag(tag);
		if(temp->get_Type() == Charater_Type::None)
			isValid = false;
		if(temp->get_Type() == Charater_Type::Tiger)
			isValid = false;
		if(temp->get_Type() == Charater_Type::Goat)
		{
			isValid = true;
			temp->setno_Character(temp->getno_Character() - 1);
			no_goat--;
			if(temp->getno_Character() == 0)
			{
				temp->set_Type(Charater_Type::None);
				temp->setTexture(Director::getInstance()->getTextureCache()->addImage("none.png"));
				temp->setOcupied(false);
			}
		}
	}

	return isValid;
}


void GamePlay::initalPos()
{

	int count_goat = 0;
	int count_tiger = 0;
	do
	{
		count_tiger += intialiseChtr(Charater_Type::Tiger, 0, 0);
	}while(no_tiger != count_tiger);

	do
	{
		count_goat += intialiseChtr(Charater_Type::Goat, 0, 4);
	}while(no_goat != count_goat);


}

int GamePlay::intialiseChtr(Charater_Type type, int no_animal, int count)
{
	int x = 0,y = 0, tag = 0;
	x = RandomHelper::random_int(0, grid_size-1);
	y = RandomHelper::random_int(0, grid_size-1);
	tag = (grid_size * x) + y;
	//	log("In the GamePlay create %d", tag);
	LocationNode* temp = (LocationNode*)(board->getChildByTag(tag));
	if(temp->getno_Character() <= count && temp->get_Type() != Charater_Type::Tiger)
	{
		temp->set_Type(type);
		temp->setTexture(Director::getInstance()->getTextureCache()->addImage("goat.png"));
		temp->setno_Character(temp->getno_Character()+1);
		temp->setOcupied(true);
		no_animal++;
	}
	if(type == Charater_Type::Tiger)
	{
		pos_lion.push_back(Vec2(x,y));
		log("In GamePlay in intialiseChtr %d  %d",x,y);
		temp->setTexture(Director::getInstance()->getTextureCache()->addImage("tiger1.png"));
	}

	return no_animal;
}

bool GamePlay::update_Tigermove(Vec2 prev, Vec2 cur)
{
	if(turn == Turn::Computer)
	{
		LocationNode* cur_temp = nullptr;;
		int cur_x = 0, cur_y = 0, cur_tag = 0;
		cur_x = cur.x;
		cur_y = cur.y;
		cur_tag = (grid_size * cur_x) + cur_y;
		cur_temp = (LocationNode*)(board->getChildByTag(cur_tag));

		LocationNode* prev_temp = nullptr;
		int prev_x = 0, prev_y = 0, prev_tag = 0;
		prev_x = prev.x;
		prev_y = prev.y;
		prev_tag = (grid_size * prev_x) + prev_y;
		prev_temp = (LocationNode*)(board->getChildByTag(prev_tag));

		if(cur_temp->get_Type() == Charater_Type::None && prev_temp->get_Type() == Charater_Type::Tiger)
		{
			log("In the GamePlay TigerMove_update %d  %d",prev_tag, cur_tag);
			prev_temp->setno_Character(0);
			prev_temp->set_Type(Charater_Type::None);
			prev_temp->setOcupied(false);
			prev_temp->setTexture(Director::getInstance()->getTextureCache()->addImage("none.png"));

			cur_temp->setno_Character(1);
			cur_temp->set_Type(Charater_Type::Tiger);
			cur_temp->setTexture(Director::getInstance()->getTextureCache()->addImage("tiger1.png"));
			cur_temp->setOcupied(true);

			return true;
		}
		else
			return false;

	}
	else
		return false;

}

bool GamePlay::tiger_Move()
{
	log("In the GamePlay TigerMove_1");
	if(no_goat <= 0)
	{
		turn = Turn::Nobody;
		MessageBox("Game Over","GeekGames");
	}
	bool sucessful_move = false;
	for(int i = 0; i < possible_moves.size(); ++i)
	{
		log("In the GamePlay TigerMove_2");

		Vec2 new_pos_0 = Vec2(pos_lion.at(0).x + possible_moves.at(i).x, pos_lion.at(0).y + possible_moves.at(i).y);
		Vec2 new_pos_1 = Vec2(pos_lion.at(1).x + possible_moves.at(i).x, pos_lion.at(1).y + possible_moves.at(i).y);
		if(validate_special_move(pos_lion.at(0), new_pos_0))
		{
			log("In the GamePlay TigerMove_3");
			if(update_Tigermove(pos_lion.at(0), new_pos_0))
			{
				sucessful_move = true;
				pos_lion.erase (pos_lion.begin()+0);
				pos_lion.push_back(new_pos_0);
				label->setString("No of Goat: "+Value(no_goat).asString());
				turn = Turn::Player;
				return sucessful_move;
			}
		}
		if(validate_special_move(pos_lion.at(1), new_pos_1))
		{
			log("In the GamePlay TigerMove_4");
			if(update_Tigermove(pos_lion.at(1), new_pos_1))
			{
				sucessful_move = true;
				pos_lion.erase (pos_lion.begin()+1);
				pos_lion.push_back(new_pos_1);
				label->setString("No of Goat: "+Value(no_goat).asString());
				turn = Turn::Player;
				return sucessful_move;
			}
		}

	}

	if(!sucessful_move)
	{

		Vec2 temp;
		log("In the GamePlay TigerMove_5");
		if(validate_general_move(pos_lion.at(0), Vec2(pos_lion.at(0).x, pos_lion.at(0).y-1)))
		{
			log("In the GamePlay TigerMove_51");
			if(update_Tigermove(pos_lion.at(0), Vec2(pos_lion.at(0).x, pos_lion.at(0).y-1)))
			{
				log("In the GamePlay TigerMove_52");
				temp.x = pos_lion.at(0).x;
				temp.y = pos_lion.at(0).y-1;
				pos_lion.erase(pos_lion.begin()+0);
				pos_lion.push_back(temp);
				sucessful_move = true;
				label->setString("No of Goat: "+Value(no_goat).asString());
				turn = Turn::Player;
				return sucessful_move;
			}
		}
		if(validate_general_move(pos_lion.at(0), Vec2(pos_lion.at(0).x, pos_lion.at(0).y+1)))
		{
			log("In the GamePlay TigerMove_53");
			if(update_Tigermove(pos_lion.at(0), Vec2(pos_lion.at(0).x, pos_lion.at(0).y+1)))
			{
				log("In the GamePlay TigerMove_54");
				temp.x = pos_lion.at(0).x;
				temp.y = pos_lion.at(0).y+1;
				pos_lion.erase(pos_lion.begin()+0);
				pos_lion.push_back(temp);
				sucessful_move = true;
				label->setString("No of Goat: "+Value(no_goat).asString());
				turn = Turn::Player;
				return sucessful_move;
			}
		}
		if(validate_general_move(pos_lion.at(0), Vec2(pos_lion.at(0).x+1, pos_lion.at(0).y)))
		{
			log("In the GamePlay TigerMove_55");
			if(update_Tigermove(pos_lion.at(0), Vec2(pos_lion.at(0).x+1, pos_lion.at(0).y)))
			{
				log("In the GamePlay TigerMove_56");
				temp.x = pos_lion.at(0).x+1;
				temp.y = pos_lion.at(0).y;
				pos_lion.erase(pos_lion.begin()+0);
				pos_lion.push_back(temp);
				sucessful_move = true;
				label->setString("No of Goat: "+Value(no_goat).asString());
				turn = Turn::Player;
				return sucessful_move;
			}
		}
		if(validate_general_move(pos_lion.at(0), Vec2(pos_lion.at(0).x-1, pos_lion.at(0).y)))
		{
			log("In the GamePlay TigerMove_57");
			if(update_Tigermove(pos_lion.at(0), Vec2(pos_lion.at(0).x-1, pos_lion.at(0).y)))
			{
				log("In the GamePlay TigerMove_58");
				temp.x = pos_lion.at(0).x-1;
				temp.y = pos_lion.at(0).y;
				pos_lion.erase(pos_lion.begin()+0);
				pos_lion.push_back(temp);
				sucessful_move = true;
				label->setString("No of Goat: "+Value(no_goat).asString());
				turn = Turn::Player;
				return sucessful_move;
			}
		}

		//////////////////////////////////////////// Lion 2-----------------------------------

		if(validate_general_move(pos_lion.at(1), Vec2(pos_lion.at(1).x, pos_lion.at(1).y-1)))
		{
			if(update_Tigermove(pos_lion.at(1), Vec2(pos_lion.at(1).x, pos_lion.at(1).y-1)))
			{
				temp.x = pos_lion.at(1).x;
				temp.y = pos_lion.at(1).y-1;
				pos_lion.erase(pos_lion.begin()+1);
				pos_lion.push_back(temp);
				sucessful_move = true;
				label->setString("No of Goat: "+Value(no_goat).asString());
				turn = Turn::Player;
				return sucessful_move;
			}
		}
		if(validate_general_move(pos_lion.at(1), Vec2(pos_lion.at(1).x, pos_lion.at(1).y+1)))
		{
			if(update_Tigermove(pos_lion.at(1), Vec2(pos_lion.at(1).x, pos_lion.at(1).y+1)))
			{
				temp.x = pos_lion.at(1).x;
				temp.y = pos_lion.at(1).y+1;
				pos_lion.erase(pos_lion.begin()+1);
				pos_lion.push_back(temp);
				sucessful_move = true;
				label->setString("No of Goat: "+Value(no_goat).asString());
				turn = Turn::Player;
				return sucessful_move;
			}
		}
		if(validate_general_move(pos_lion.at(1), Vec2(pos_lion.at(1).x+1, pos_lion.at(1).y)))
		{
			if(update_Tigermove(pos_lion.at(1), Vec2(pos_lion.at(1).x+1, pos_lion.at(1).y)))
			{
				temp.x = pos_lion.at(1).x+1;
				temp.y = pos_lion.at(1).y;
				pos_lion.erase(pos_lion.begin()+1);
				pos_lion.push_back(temp);
				sucessful_move = true;
				label->setString("No of Goat: "+Value(no_goat).asString());
				turn = Turn::Player;
				return sucessful_move;
			}
		}
		if(validate_general_move(pos_lion.at(1), Vec2(pos_lion.at(1).x-1, pos_lion.at(1).y)))
		{
			if(update_Tigermove(pos_lion.at(1), Vec2(pos_lion.at(1).x-1, pos_lion.at(1).y)))
			{
				temp.x = pos_lion.at(1).x-1;
				temp.y = pos_lion.at(1).y;
				pos_lion.erase(pos_lion.begin()+1);
				pos_lion.push_back(temp);
				sucessful_move = true;

				label->setString("No of Goat: "+Value(no_goat).asString());
				turn = Turn::Player;
				return sucessful_move;
			}
		}

	}
	label->setString("No of Goat: "+Value(no_goat).asString());
	turn = Turn::Player;
	return sucessful_move;

}

void GamePlay::onUserMove(LocationNode* first, LocationNode * second)
{
	log("In the GamePlay userMove_1");
	if(turn == Turn::Player)
	{
		log("In the GamePlay userMove_2");
		if(validate_general_move(first->getnod_Cordinate(), second->getnod_Cordinate()))
		{
			log("In the GamePlay userMove_3");
			if(second->get_Type() != Charater_Type::Tiger && first->get_Type() == Charater_Type::Goat)
			{
				log("In the GamePlay userMove_4");
				first->setno_Character(first->getno_Character()-1);

				if(first->getno_Character() == 0)
				{
					log("In the GamePlay userMove_5");
					first->set_Type(Charater_Type::None);
					first->setTexture(Director::getInstance()->getTextureCache()->addImage("none.png"));
					first->setOcupied(false);
				}

				second->set_Type(Charater_Type::Goat);
				second->setTexture(Director::getInstance()->getTextureCache()->addImage("goat.png"));
				second->setno_Character(second->getno_Character()+1);
				second->setOcupied(true);
				board->move_count = 0;
				board->locNodeList.erase(board->locNodeList.begin(), board->locNodeList.end());
				log("In the GamePlay userMove_6");
				turn = Turn::Computer;
				isGameOver = tiger_Move();
				log("In the GamePlay userMove_7");
			}
			else
			{
				board->move_count = 0;
				board->locNodeList.erase(board->locNodeList.begin(), board->locNodeList.end());
			}

		}
		else
		{
			board->move_count = 0;
			board->locNodeList.erase(board->locNodeList.begin(), board->locNodeList.end());
		}
	}
	else if(turn != Turn::Nobody)
	{
		isGameOver = tiger_Move();
	}

	if(!isGameOver)
	{
		turn = Turn::Nobody;
		MessageBox("Game Over","GeekGames");

	}
}
