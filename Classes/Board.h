/*
 * Board.h
 *
 *  Created on: Oct 17, 2015
 *      Author: SINGHAK
 */

#ifndef BOARD_H_
#define BOARD_H_

#include "cocos2d.h"
#include "LocationNode.h"
USING_NS_CC;

class Board : public Layer
{

private:
	int no_grid;
	LocationNode* loc_node;
	Size size;
	int stepX, stepY;
//	GamePlay::Turn turn;
	Vec2 setLocNodePos(int x, int y, int y_chang, LocationNode* loc_1);
public:
	Vector<LocationNode*> locNodeList;
	int move_count;
	Board();
	virtual ~Board();

	void setno_Grid(int no_grid);
	int getno_Grid();
	void setNodeChild(LocationNode* loc_node);
//	LocationNode* getNodeChild();


	void createGrid();

	CREATE_FUNC(Board);
};

#endif /* BOARD_H_ */
