/*
 * LocationNode.h
 *
 *  Created on: Oct 17, 2015
 *      Author: SINGHAK
 */

#ifndef LOCATIONNODE_H_
#define LOCATIONNODE_H_

#include "cocos2d.h"
USING_NS_CC;

enum Charater_Type { None, Tiger, Goat};
class LocationNode : public Sprite
{
private:

	Size size;
	Vec2 position;
	bool isOcupied;
	int no_Characte;
	Charater_Type charater;
	Label* label;
	void setLabel();
	void updateLabel();


public:
	LocationNode();
	virtual ~LocationNode();
	static LocationNode* create();
	void setnod_Cordinate(Vec2 postion);
	Vec2 getnod_Cordinate();
	bool is_Ocupied();
	void setOcupied(bool occupied);
	void setno_Character(int no_charater);
	int getno_Character();
	void set_Type(Charater_Type type);
	Charater_Type get_Type();
	void addEvents();

};

#endif /* LOCATIONNODE_H_ */
