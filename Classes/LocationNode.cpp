/*
 * LocationNode.cpp
 *
 *  Created on: Oct 17, 2015
 *      Author: SINGHAK
 */

#include "LocationNode.h"
#include "Board.h"

LocationNode::LocationNode()
{
	// TODO Auto-generated constructor stub
	no_Characte = 0;
	isOcupied = false;
	charater = Charater_Type::None;
	size = Director::getInstance()->getWinSize();
}

LocationNode::~LocationNode()
{
	// TODO Auto-generated destructor stub
}

LocationNode* LocationNode::create()
{
	LocationNode* location = new LocationNode();

	if (location && location->initWithFile("none.png"))
	{
		location->autorelease();

		location->addEvents();
		location->setLabel();
		return location;
	}

	CC_SAFE_DELETE(location);
	return NULL;
}

void LocationNode::setnod_Cordinate(Vec2 postion)
{
	this->position = postion;
}

Vec2 LocationNode::getnod_Cordinate()
{
	return position;
}

void LocationNode::setOcupied(bool ocupied)
{
	this->isOcupied = ocupied;
}

bool LocationNode::is_Ocupied()
{
	return isOcupied;
}

void LocationNode::set_Type(Charater_Type type)
{
	this->charater = type;
}

Charater_Type LocationNode::get_Type()
{
	return charater;
}

void LocationNode::setno_Character(int no_charater)
{
	this->no_Characte = no_charater;
	updateLabel();
}

int LocationNode::getno_Character()
{
	return no_Characte;
}

void LocationNode::addEvents()
{
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);

    listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event)
    {
        Vec2 p = this->getParent()->convertToNodeSpace(touch->getLocation());
        Rect rect = this->getBoundingBox();


        if(rect.containsPoint(p))
        {
//        	((Board*)( this->getParent()))->setNodeChild(this);
            return true; // to indicate that we have consumed it.
        }

        return false; // we did not consume this event, pass thru.
    };

    listener->onTouchEnded = [&](cocos2d::Touch* touch, cocos2d::Event* event)
    {
      ((Board*)( this->getParent()))->setNodeChild(this);
    };

    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}

void LocationNode::setLabel()
{
	label = Label::createWithTTF(Value(no_Characte).asString(),"fonts\arial.ttf", 35);
	label->retain();
	label->setColor(Color3B::BLUE);
	Size size = this->getBoundingBox().size;
	label->setPosition(size.width/2, size.height/2);
	this->addChild(label);
}

void LocationNode::updateLabel()
{
//	if(label)
	log("In the Location updateLabel %d",no_Characte);
		label->setString(Value(no_Characte).asString());
}
